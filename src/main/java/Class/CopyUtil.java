package Class;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class CopyUtil {

	
	public  static String copyLargeToString(InputStream input) throws IOException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		copyLarge(input, output);
		
		return new String(output.toByteArray(), "UTF-8");
    }

	private static long copyLarge(InputStream input, OutputStream output)
		       throws IOException {
		   byte[] buffer = new byte[1024];
		   long count = 0;
		   int n = 0;
		   while (-1 != (n = input.read(buffer))) {
		       output.write(buffer, 0, n);
		       count += n;
		   }
		   return count;
		}
	
	
	
	
	
	public  List<ParseJson> ParserTe(InputStream in) throws JSONException, IOException {
		List<ParseJson>list = new ArrayList<ParseJson>();

       	JSONObject obj = new JSONObject(copyLargeToString(in));

		JSONArray dataArray =(JSONArray) obj.getJSONArray("elements");
		
        for (int i = 0; i < dataArray.length(); i++) {
        	

			JSONObject jsonalbum = dataArray.getJSONObject(i);
			ParseJson p = new ParseJson();
		     
		     p.setError_id((String.valueOf(jsonalbum.getLong("id"))));
		     p.setLat((String.valueOf(jsonalbum.getFloat("lat"))));
		     p.setLon((String.valueOf(jsonalbum.getFloat("lon"))));
		     
		     obj.put("direction", "Forward");
		     System.out.print(obj);
		     
		     //JSONObject artistejson= jsonalbum.getJSONObject("artist");

			//artiste.setId(String.valueOf(artistejson.getLong("id")));

		     //p.setLon((jsonalbum.getString("lon")));
		     //p.setError_id((jsonalbum.getString("error_id")));
		     //p.setItem((jsonalbum.getString("item")));

            //String id = dataArray.getJSONObject(i).getString("id");
            
          //  System.out.println(id);
            
            
            list.add(p);
        }
	
	return list;
	
	}
}
