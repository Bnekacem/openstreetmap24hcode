package Class;

public class Panneau {
	
	
	
	private int id;
	
	
	private String feature_0_user_key;
	private String feature_1_user_key;
	private String feature_0_sequence_key;
	private String feature_1_sequence_key;
	
	private String lat;
	
	private String lon;
	
	private String highway;
	
	private String source;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getHighway() {
		return highway;
	}

	public void setHighway(String highway) {
		this.highway = highway;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Panneau(int id, String highway, String source) {
		this.id = id;
	
		this.highway = highway;
		this.source = source;
	}
	public void AjoutPanneau(int id, String lat, String lon) {
		this.id = id;
	
		this.lat = lat;
		this.lon = lon;
	}

	
public void trouverdirection(long Cordinate_00, long Cordinate_10) {
	
	if ((feature_0_user_key==feature_1_user_key) && ((feature_0_sequence_key==feature_1_sequence_key))){
		if (Cordinate_00==Cordinate_10) {
			this.highway="forward";
		}else {
			this.highway="backward";
		}
	}
}
	
}
