package Class;

import java.io.FileInputStream;
import java.io.InputStream;

import org.json.JSONArray;
import org.json.JSONObject;

public class ParseJson {
	
	
	private String lat ;
	private String lon ;
	private String error_id;
	private String item ;
	
	
	public String getLat() {
		return lat;
	}
	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLon() {
		return lon;
	}
	public void setLon(String lon) {
		this.lon = lon;
	}
	public String getError_id() {
		return error_id;
	}
	public void setError_id(String error_id) {
		this.error_id = error_id;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	@Override
	public String toString() {
		return "ParseJson [lat=" + lat + ", lon=" + lon + ", error_id=" + error_id + ", item=" + item + "]";
	}
	

		
	}

	
	

