package Class;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;

public class ParserTest {



    public static void main(String[] args) throws JSONException, IOException {
    	
    	HttpClient client = new DefaultHttpClient();
    	  HttpGet request = new HttpGet("http://overpass-api.de/api/interpreter?data=%2F*%0AThis%20has%20been%20generated%20by%20the%20overpass-turbo%20wizard.%0AThe%20original%20search%20was%3A%0A%E2%80%9Ctype%3Anode%20and%20highway%3Dstop%20and%20direction%21%3D*%E2%80%9D%0A*%2F%0A%5Bout%3Ajson%5D%5Btimeout%3A25%5D%3B%0A%2F%2F%20gather%20results%0A%28%0A%20%20%2F%2F%20query%20part%20for%3A%20%E2%80%9Chighway%3Dstop%20and%20direction%21%3D*%E2%80%9D%0A%20%20node%5B%22highway%22%3D%22stop%22%5D%5B%22direction%22%21~%22.*%22%5D%2845.752822196649085%2C4.836430549621582%2C45.76288260933303%2C4.854540824890137%29%3B%0A%29%3B%0A%2F%2F%20print%20results%0Aout%20body%3B%0A%3E%3B%0Aout%20skel%20qt%3B");
    	  HttpResponse response = client.execute(request);
    	  BufferedReader rd = new BufferedReader (new InputStreamReader(response.getEntity().getContent()));
    	  String line = "";
    	//  FileWriter fstream = new FileWriter("out.txt");
    	  //  BufferedWriter out = new BufferedWriter(fstream);
    	    //out.write("Hello World !");
    	  while ((line = rd.readLine()) != null) {
    	    System.out.println(line);
    	  }
    	 }
    
    		  
    		 
    
	

}
